﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeNA_Prep_Tool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "Office files (*.xlsx;*.xlsm)|*.xlsx;*.xlsm|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    excelbox.Text = openFileDialog.FileName;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog folderBrowser = new OpenFileDialog())
            {
                // Set validate names and check file exists to false otherwise windows will
                // not let you select "Folder Selection."
                folderBrowser.ValidateNames = false;
                folderBrowser.CheckFileExists = false;
                folderBrowser.CheckPathExists = true;
                // Always default to Folder Selection.
                folderBrowser.FileName = "Folder Selection.";
                if (folderBrowser.ShowDialog() == DialogResult.OK)
                {
                    string folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                    glossarybox.Text = folderPath;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            //Console.WriteLine("Copy and Paste file to prepare for memoQ");
            //just feed it a single file to split
            string @abspathtofile = excelbox.Text;

            abspathtofile = abspathtofile.Replace("\"", "");

            //Create a directory for the HTML files
            string oldpath = new FileInfo(abspathtofile).DirectoryName;
            string newpath = Path.Combine(oldpath, "HTML");

            if (!Directory.Exists(newpath))
            {
                Directory.CreateDirectory(newpath);
            }

            //Console.WriteLine("Copy Path to Glossaries");
            string @glossarypath = glossarybox.Text;

            glossarypath = glossarypath.Replace("\"", "");


            string[] glossaries = Directory.GetFiles(glossarypath, "*.xlsx");

            List<DenaGlossary> glossary = new List<DenaGlossary>();
            foreach (string glossaryFile in glossaries)
            {
                ExcelPackage excel = new ExcelPackage(new FileInfo(glossaryFile));
                ExcelWorksheet ws = excel.Workbook.Worksheets[0];
                for (int row = 1; row < ws.Dimension.Rows + 1; row++)
                {
                    string[] cell = {

                             "" //ja
                            ,"" //en
                            ,"" //fr
                            ,"" //it
                            ,"" //de
                            ,"" //es
                            ,"" //ko
                            ,"" //tw
                            ,"" //DeNAID
                        };

                    int coln = 0;
                    for (int col = 5; col <= 13; col++)
                    {
                        if (ws.Cells[row, col].Value != null)
                        {
                            cell[coln] = ws.Cells[row, col].Value.ToString();
                        }
                        coln++;
                    }

                    glossary.Add(new DenaGlossary(cell[8], cell[0], cell[1], cell[2], cell[3], cell[4], cell[5], cell[6], cell[7]));
                }

            }



            ExcelPackage excelsrc = new ExcelPackage(new FileInfo(abspathtofile));

            List<refs> ingame_refs = new List<refs>();
            List<refs> annoucements_refs = new List<refs>();

            int ingame_id = 0;
            int annoucements_id = 0;
            foreach (ExcelWorksheet ws in excelsrc.Workbook.Worksheets)
            {
                //load references
                if (ws.Name.ToLower().StartsWith("ref_"))
                {
                    Console.WriteLine($"{ws.Name}");
                    for (int row = 2; row < ws.Dimension.Rows + 1; row++)
                    {
                        string[] cell = {
                             "" //type
                            ,"" //stringid
                            ,"" //locID
                            ,"" //JPN Comment
                            ,"" //ja
                            ,"" //en
                            ,"" //fr
                            ,"" //it
                            ,"" //de
                            ,"" //es
                            ,"" //ko
                            ,"" //tw
                        };

                        string replacements = "";
                        if (ws.Cells[row, 4].Value != null)
                        {
                            replacements = ws.Cells[row, 4].Value.ToString();
                        }
                        //quickly get the replacements


                        for (int col = 1; col < ws.Dimension.Columns + 1; col++)
                        {
                            if (ws.Cells[row, col].Value != null)
                            {

                                if (col - 1 < 12)
                                {

                                    //find and replace
                                    string sentence = ws.Cells[row, col].Value.ToString();
                                    int lang = col;

                                    sentence = replaceME(sentence, replacements, glossary, lang);

                                    cell[col - 1] = sentence;

                                }

                            }
                        }

                        if (ws.Name.ToLower().Contains("in-game"))
                        {


                            ingame_refs.Add(new refs(ingame_id, cell[0], cell[1], cell[2], cell[3], cell[4], cell[5], cell[6], cell[7], cell[8], cell[9], cell[10], cell[11]));
                            ingame_id++;
                        }

                        if (ws.Name.ToLower().Contains("announcements"))
                        {
                            annoucements_refs.Add(new refs(annoucements_id, cell[0], cell[1], cell[2], cell[3], cell[4], cell[5], cell[6], cell[7], cell[8], cell[9], cell[10], cell[11]));
                            annoucements_id++;
                        }
                    }




                }

            }



            foreach (ExcelWorksheet ws in excelsrc.Workbook.Worksheets)
            {

                if (ws.Name.ToLower().StartsWith("announcements"))
                {

                    //flag all of the segments we want to edit to our object
                    List<edits> fortranslation = new List<edits>();
                    int rown = 0;
                    for (int row = 2; row < ws.Dimension.Rows + 1; row++)
                    {
                        if (ws.Cells[row, 2].Value != null && ws.Cells[row, 3].Value != null)
                        {
                            string stringID = ws.Cells[row, 2].Value.ToString();
                            string locID = ws.Cells[row, 3].Value.ToString();
                            string dena_comment = "";
                            string character_limit = "N/A";

                            if (ws.Cells[row, 7].Value != null)
                            {
                                dena_comment = ws.Cells[row, 7].Value.ToString();
                            }
                            if (ws.Cells[row, 6].Value != null)
                            {
                                character_limit = ws.Cells[row, 6].Value.ToString();
                            }

                            foreach (refs reference in annoucements_refs.Where(x => x.StringID == stringID).Where(y => y.LocID == locID))
                            {

                                fortranslation.Add(new edits(rown, stringID, locID, dena_comment, character_limit));
                                rown++;
                            }
                        }
                    }

                    for (int figs = 0; figs < 7; figs++)
                    {

                        //build html previews of the files we want to edit
                        StringBuilder figsHTML = new StringBuilder();
                        figsHTML.AppendLine("<html>");
                        figsHTML.AppendLine("<body>");
                        figsHTML.AppendLine("<table style=\"border:0px solid #000000;border-collapes:collapes;width: 100%;font-family: arial;font-size:9.5pt;\">");
                        foreach (refs segments in annoucements_refs)
                        {

                            var tra = fortranslation.Where(x => x.StringID == segments.StringID).FirstOrDefault();

                            if (tra != null)
                            {
                                if (figs < 4)
                                {
                                    figsHTML.AppendLine($"" +
                                    $"<tr style=\"background-color: #FFFF00\">" +
                                        $"<td style=\"width: 25%\">" +
                                            $"<table style=\"border:0px solid #000000;width: 100%;font-family: arial;font-size:9.5pt;\">" +
                                                $"<tr>" +
                                                    $"<td style=\"width: 50%\">StringID</td>" +
                                                    $"<td style=\"width: 50%\">LocID:</td>" +
                                                $"</tr>" +
                                                $"<tr>" +
                                                    $"<td>{segments.StringID}</td>" +
                                                    $"<td>{segments.LocID}</td>" +
                                                $"</tr>" +
                                            $"</table>" +
                                        $"</td>" +
                                        $"<td style=\"width: 50%\">{segments.EN}</td>" +
                                        $"<td style=\"width: 25%\">" +
                                            $"<table style=\"border:0px solid #000000;width: 100%;font-family: arial;font-size:9.5pt;\">" +
                                                $"<tr>" +
                                                    $"<td style=\"width: 50%\">Comments DeNA</td>" +
                                                    $"<td style=\"width: 50%\">Character Limit</td>" +
                                                $"</tr>" +
                                                $"<tr>" +
                                                    $"<td>{tra.DenaComments}</td>" +
                                                    $"<td>{tra.CharacterLimit}</td>" +
                                                $"</tr>" +
                                            $"</table>" +
                                        $"</td>" +
                                    $"</tr>");
                                }
                                else
                                {
                                    figsHTML.AppendLine($"" +
                                    $"<tr style=\"background-color: #FFFF00\">" +
                                        $"<td style=\"width: 25%\">" +
                                            $"<table style=\"border:0px solid #000000;width: 100%;font-family: arial;font-size:9.5pt;\">" +
                                                $"<tr>" +
                                                    $"<td style=\"width: 50%\">StringID</td>" +
                                                    $"<td style=\"width: 50%\">LocID:</td>" +
                                                $"</tr>" +
                                                $"<tr>" +
                                                    $"<td>{segments.StringID}</td>" +
                                                    $"<td>{segments.LocID}</td>" +
                                                $"</tr>" +
                                            $"</table>" +
                                        $"</td>" +
                                        $"<td style=\"width: 50%\">{segments.JA}</td>" +
                                        $"<td style=\"width: 25%\">" +
                                            $"<table style=\"border:0px solid #000000;width: 100%;font-family: arial;font-size:9.5pt;\">" +
                                                $"<tr>" +
                                                    $"<td style=\"width: 50%\">Comments DeNA</td>" +
                                                    $"<td style=\"width: 50%\">Character Limit</td>" +
                                                $"</tr>" +
                                                $"<tr>" +
                                                    $"<td>{tra.DenaComments}</td>" +
                                                    $"<td>{tra.CharacterLimit}</td>" +
                                                $"</tr>" +
                                            $"</table>" +
                                        $"</td>" +
                                    $"</tr>");
                                }



                            }
                            else
                            {
                                //FR
                                if (figs == 0)
                                {
                                    figsHTML.AppendLine(
                                        $"<tr>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                            $"<td style=\"width: 50%\">{segments.FR}</td>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                        $"</tr>");
                                }
                                if (figs == 1)
                                {
                                    figsHTML.AppendLine(
                                        $"<tr>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                            $"<td style=\"width: 50%\">{segments.IT}</td>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                        $"</tr>");
                                }
                                if (figs == 2)
                                {
                                    figsHTML.AppendLine(
                                        $"<tr>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                            $"<td style=\"width: 50%\">{segments.DE}</td>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                        $"</tr>");
                                }
                                if (figs == 3)
                                {
                                    figsHTML.AppendLine(
                                        $"<tr>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                            $"<td style=\"width: 50%\">{segments.ES}</td>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                        $"</tr>");
                                }
                                if (figs == 4)
                                {
                                    figsHTML.AppendLine(
                                        $"<tr>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                            $"<td style=\"width: 50%\">{segments.KO}</td>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                        $"</tr>");
                                }
                                if (figs == 5)
                                {
                                    figsHTML.AppendLine(
                                        $"<tr>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                            $"<td style=\"width: 50%\">{segments.TW}</td>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                        $"</tr>");
                                }
                                if (figs == 6)
                                {
                                    figsHTML.AppendLine(
                                        $"<tr>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                            $"<td style=\"width: 50%\">{segments.EN}</td>" +
                                            $"<td style=\"width: 25%\"></td>" +
                                        $"</tr>");
                                }
                            }

                        }
                        figsHTML.AppendLine("</table>");
                        figsHTML.AppendLine("</body>");
                        figsHTML.AppendLine("</html>");
                        if (figs == 0)
                        {
                            string xfile = new FileInfo(abspathtofile.Replace(".xlsx", "_FR.html")).Name;

                            File.WriteAllText(Path.Combine(newpath,xfile), figsHTML.ToString());
                        }
                        if (figs == 1)
                        {
                            string xfile = new FileInfo(abspathtofile.Replace(".xlsx", "_IT.html")).Name;
                            File.WriteAllText(Path.Combine(newpath, xfile), figsHTML.ToString());
                        }
                        if (figs == 2)
                        {
                            string xfile = new FileInfo(abspathtofile.Replace(".xlsx", "_DE.html")).Name;
                            File.WriteAllText(Path.Combine(newpath, xfile), figsHTML.ToString());
                        }
                        if (figs == 3)
                        {
                            string xfile = new FileInfo(abspathtofile.Replace(".xlsx", "_ES.html")).Name;
                            File.WriteAllText(Path.Combine(newpath, xfile), figsHTML.ToString());
                        }
                        if (figs == 4)
                        {
                            string xfile = new FileInfo(abspathtofile.Replace(".xlsx", "_KO.html")).Name;
                            File.WriteAllText(Path.Combine(newpath, xfile), figsHTML.ToString());
                        }
                        if (figs == 5)
                        {
                            string xfile = new FileInfo(abspathtofile.Replace(".xlsx", "_TW.html")).Name;
                            File.WriteAllText(Path.Combine(newpath, xfile), figsHTML.ToString());
                        }
                        if (figs == 6)
                        {
                            string xfile = new FileInfo(abspathtofile.Replace(".xlsx", "_EN.html")).Name;
                            File.WriteAllText(Path.Combine(newpath, xfile), figsHTML.ToString());
                        }
                    }

                }

                if (ws.Name.ToLower().StartsWith("in-game"))
                {
                    ws.Cells[1, 15].Value = "JP_Comment";
                    ws.Cells[1, 16].Value = "EN_Comment";
                    ws.Cells[1, 17].Value = "FR_Comment";
                    ws.Cells[1, 18].Value = "IT_Comment";
                    ws.Cells[1, 19].Value = "DE_Comment";
                    ws.Cells[1, 20].Value = "ES_Comment";
                    ws.Cells[1, 21].Value = "KO_Comment";
                    ws.Cells[1, 22].Value = "TW_Comment";

                    for (int row = 2; row < ws.Dimension.Rows + 1; row++)
                    {
                        if (ws.Cells[row, 7].Value != null)
                        {
                            string comments_DeNA = ws.Cells[row, 7].Value.ToString();
                            //Console.WriteLine(comments_DeNA);
                            string pattern = @"([a-zA-Z_])+\/p?\d+";
                            MatchCollection matches = Regex.Matches(comments_DeNA, pattern);

                            StringBuilder jpn = new StringBuilder();
                            StringBuilder eng = new StringBuilder();
                            StringBuilder fre = new StringBuilder();
                            StringBuilder ita = new StringBuilder();
                            StringBuilder ger = new StringBuilder();
                            StringBuilder spa = new StringBuilder();
                            StringBuilder kor = new StringBuilder();
                            StringBuilder ztw = new StringBuilder();

                            foreach (Match match in matches)
                            {
                                //Console.WriteLine($"{match.Value}");
                                foreach (refs reference in ingame_refs.Where(x => x.StringID == match.Value))
                                {
                                    //Console.WriteLine($"{reference.StringID}::{reference.JA}::{reference.EN}");
                                    jpn.Append(reference.JA);
                                    jpn.Append("\n");
                                    jpn.Append(reference.StringID);
                                    jpn.Append("\n");

                                    eng.Append(reference.EN);
                                    eng.Append("\n");
                                    eng.Append(reference.StringID);
                                    eng.Append("\n");

                                    fre.Append(reference.FR);
                                    fre.Append("\n");
                                    fre.Append(reference.StringID);
                                    fre.Append("\n");

                                    ita.Append(reference.IT);
                                    ita.Append("\n");
                                    ita.Append(reference.StringID);
                                    ita.Append("\n");

                                    ger.Append(reference.DE);
                                    ger.Append("\n");
                                    ger.Append(reference.StringID);
                                    ger.Append("\n");

                                    spa.Append(reference.ES);
                                    spa.Append("\n");
                                    spa.Append(reference.StringID);
                                    spa.Append("\n");

                                    kor.Append(reference.KO);
                                    kor.Append("\n");
                                    kor.Append(reference.StringID);
                                    kor.Append("\n");

                                    ztw.Append(reference.TW);
                                    ztw.Append("\n");
                                    ztw.Append(reference.StringID);
                                    ztw.Append("\n");


                                }
                            }

                            ws.Cells[row, 15].Value = jpn.ToString();
                            ws.Cells[row, 16].Value = eng.ToString();
                            ws.Cells[row, 17].Value = fre.ToString();
                            ws.Cells[row, 18].Value = ita.ToString();
                            ws.Cells[row, 19].Value = ger.ToString();
                            ws.Cells[row, 20].Value = spa.ToString();
                            ws.Cells[row, 21].Value = kor.ToString();
                            ws.Cells[row, 22].Value = ztw.ToString();
                        }
                    }
                }
            }

            //clean janky things in the file
            foreach (ExcelWorksheet ws in excelsrc.Workbook.Worksheets)
            {
                if (ws.Dimension != null)
                {
                    for (int row = 1; row < ws.Dimension.Rows + 1; row++)
                    {
                        for (int col = 1; col < ws.Dimension.Columns + 1; col++)
                        {
                            if (ws.Cells[row, col].Value != null)
                            {
                                ws.Cells[row, col].Value = ws.Cells[row, col].Text.Replace("\r", "");
                            }
                        }

                    }
                }
                
            }

            excelsrc.SaveAs(new FileInfo(abspathtofile.Replace(".xlsx", "_for_import.xlsx")));
            MessageBox.Show("Done!");
        }


        static string replaceME(string sentence, string replacements, List<DenaGlossary> glossary, int lang)
        {
            if (lang < 5 || lang > 12)
            {
                return sentence;
            }

            string pattern = @"([a-zA-Z_0-9])+\/([a-zA-Z_0-9])+";

            MatchCollection matches = Regex.Matches(replacements, pattern);

            foreach (Match match in matches)
            {
                //Console.WriteLine($"{match.Value}");
                var term = glossary.Where(x => x.DenaID == match.Value).FirstOrDefault();
                if (term != null)
                {
                    //the variable's reference gets replaced with a value from the glossary

                    if (lang == 5)
                    {
                        replacements = Regex.Replace(replacements, match.Value, term.JA);
                    }
                    if (lang == 6)
                    {
                        replacements = Regex.Replace(replacements, match.Value, term.EN);
                    }
                    if (lang == 7)
                    {
                        replacements = Regex.Replace(replacements, match.Value, term.FR);
                    }
                    if (lang == 8)
                    {
                        replacements = Regex.Replace(replacements, match.Value, term.IT);
                    }
                    if (lang == 9)
                    {
                        replacements = Regex.Replace(replacements, match.Value, term.DE);
                    }
                    if (lang == 10)
                    {
                        replacements = Regex.Replace(replacements, match.Value, term.ES);
                    }
                    if (lang == 11)
                    {
                        replacements = Regex.Replace(replacements, match.Value, term.KO);

                    }
                    if (lang == 12)
                    {
                        replacements = Regex.Replace(replacements, match.Value, term.TW);
                    }
                }


            }

            string idx_pattern1 = @"(ID_F\d+)(=)(.*?)$";
            string idx_pattern2 = @"(ID_\d+)(=)(.*?)$";
            string idx_pattern3 = @"(ID_\d+ormore)(=)(.*?)$";

            //Console.WriteLine(replacements);
            string[] ID_xs = replacements.Split('\n');

            //this is silly but I don't care
            for (int i = 0; i < ID_xs.Count(); i++)
            {

                string sub_pattern_find = Regex.Replace(ID_xs[i], idx_pattern1, "$1");
                string sub_pattern_replace = Regex.Replace(ID_xs[i], idx_pattern1, "<b style=\"color: #0000ff\">$3</b>");

                if (sub_pattern_find != "" && sub_pattern_replace != "")
                {
                    sentence = sentence.Replace(sub_pattern_find, sub_pattern_replace);
                }

                sub_pattern_find = Regex.Replace(ID_xs[i], idx_pattern2, "$1");
                sub_pattern_replace = Regex.Replace(ID_xs[i], idx_pattern2, "<b style=\"color: #0000ff\">$3</b>");

                if (sub_pattern_find != "" && sub_pattern_replace != "")
                {
                    sentence = sentence.Replace(sub_pattern_find, sub_pattern_replace);
                }

                sub_pattern_find = Regex.Replace(ID_xs[i], idx_pattern3, "$1");
                sub_pattern_replace = Regex.Replace(ID_xs[i], idx_pattern3, "<b style=\"color: #0000ff\">$3</b><i>");

                if (sub_pattern_find != "" && sub_pattern_replace != "")
                {
                    sentence = sentence.Replace(sub_pattern_find, sub_pattern_replace);
                }

                sentence = sentence.Replace("{{#_}}", "<b style=\"color: #0000ff\">");
                sentence = sentence.Replace("{{/_}}", "</b>");


            }


            return sentence;
        }


    }


    class DenaGlossary
    {
        private string denaID;
        private string ja;
        private string en;
        private string fr;
        private string it;
        private string de;
        private string es;
        private string ko;
        private string tw;

        public DenaGlossary(string denaID, string ja, string en, string fr, string it, string de, string es, string ko, string tw)
        {
            this.denaID = denaID;
            this.ja = ja;
            this.en = en;
            this.fr = fr;
            this.it = it;
            this.de = de;
            this.es = es;
            this.ko = ko;
            this.tw = tw;
        }
        public string DenaID
        {
            get { return denaID; }
        }
        public string JA
        {
            get { return ja; }
        }
        public string EN
        {
            get { return en; }
        }
        public string FR
        {
            get { return fr; }
        }
        public string IT
        {
            get { return it; }
        }
        public string DE
        {
            get { return de; }
        }
        public string ES
        {
            get { return es; }
        }
        public string KO
        {
            get { return ko; }
        }
        public string TW
        {
            get { return tw; }
        }
    }
    class edits
    {
        private int cid;
        private string version;
        private string stringID;
        private string locID;
        private string ms_comment;
        private string ja;
        private string character_limit;
        private string dena_comments;
        private string en;
        private string fr;
        private string it;
        private string de;
        private string es;
        private string ko;
        private string tw;

        public edits(int cid, string stringID, string locID, string dena_comments, string character_limit)
        {
            this.cid = cid;
            this.stringID = stringID;
            this.locID = locID;
            this.dena_comments = dena_comments;
            this.character_limit = character_limit;
        }

        public string StringID
        {
            get { return stringID; }
        }

        public string LocID
        {
            get { return locID; }
        }

        public string DenaComments
        {
            get { return dena_comments; }
        }

        public string CharacterLimit
        {
            get { return character_limit; }
        }
    }

    class refs
    {
        private int cid;
        private string type;
        private string stringid;
        private string locid;
        private string comment;
        private string ja_ref;
        private string en_ref;
        private string fr_ref;
        private string it_ref;
        private string de_ref;
        private string es_ref;
        private string ko_ref;
        private string tw_ref;

        public refs(int cid, string type, string stringid, string locID, string comment, string ja, string en, string fr, string it, string de, string es, string ko, string tw)
        {
            this.cid = cid;
            this.type = type;
            this.stringid = stringid;
            this.locid = locID;
            this.comment = comment;
            this.ja_ref = ja;
            this.en_ref = en;
            this.fr_ref = fr;
            this.it_ref = it;
            this.de_ref = de;
            this.es_ref = es;
            this.ko_ref = ko;
            this.tw_ref = tw;
        }

        public int CID
        {
            get { return cid; }
        }
        public string Type
        {
            get { return type; }
        }
        public string StringID
        {
            get { return stringid; }
        }
        public string LocID
        {
            get { return locid; }
        }
        public string Comment
        {
            get { return comment; }
        }
        public string JA
        {
            get { return ja_ref; }
        }
        public string EN
        {
            get { return en_ref; }
        }
        public string FR
        {
            get { return fr_ref; }
        }
        public string IT
        {
            get { return it_ref; }
        }
        public string DE
        {
            get { return de_ref; }
        }
        public string ES
        {
            get { return es_ref; }
        }
        public string KO
        {
            get { return ko_ref; }
        }
        public string TW
        {
            get { return tw_ref; }
        }
    }
}
